#!/bin/bash

#pre-build
rm -rf Build

#build
echo "============= App built ==============="
mkdir Build
cd Build
cmake .. > /dev/null
make # > /dev/null

#clean
cd ..
#rm -rf Build
