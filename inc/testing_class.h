#include <string>

class foo {
public:
	foo(const std::string &msg);
	const std::string &get_message();
private:
	std::string msg;
};
