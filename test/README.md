How to use manually:

1 set env variable "TIZEN_SDK_HOME" to your tizen-studio location:
export TIZEN_SDK_HOME ="/home/geusfexx/ide/tizen-studio"

2 install required tools
sudo apt-get install gcc g++ cmake make

3 launch test running script
./run.sh

4 grab xml reports from "reports" directory
