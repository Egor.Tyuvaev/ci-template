#!/bin/bash

#pre-build
export GTEST_OUTPUT="xml:../reports/"
set -e
set -o pipefail

tar -xvf googletest-1.7.0.tar.gz > /dev/null
rm -rf build

#build
mkdir build
cd build
cmake .. > /dev/null
make all # > /dev/null

#run tests
for FILE in *
do
	if [[ $FILE == *"tests.out"  ]]
	then
		./$FILE
	fi
done

#clean
cd ..
rm -rf googletest
